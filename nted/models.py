from __future__ import unicode_literals
from xml.dom import minidom

from django.core.validators import RegexValidator
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from .constants import COUNTRIES, SUFFIXES, US_STATES, US_TERRITORIES, US_MILITARY, KSA_CHOICES, SURVEY_CHOICES


@python_2_unicode_compatible
class NtedTrainingProvider(models.Model):
    """
    """
    abbreviation = models.CharField(max_length=25, unique=True)
    institution = models.CharField(max_length=255)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['abbreviation']

    def __str__(self):
        return self.abbreviation

    def get_res_element(self, doc):
        element = doc.createElement('trainingprovider')
        element.setAttribute('tpid', self.abbreviation)
        element.setAttribute('tpphone', self.phone)
        element.setAttribute('tpemail', self.email)
        return element


@python_2_unicode_compatible
class NtedAudienceType(models.Model):
    """
    """
    abbreviation = models.CharField(max_length=5, unique=True)
    discipline = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['abbreviation']

    def __str__(self):
        return self.discipline


@python_2_unicode_compatible
class NtedTrainingMethod(models.Model):
    """
    """
    code = models.CharField(max_length=1, unique=True)
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['code']

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)


@python_2_unicode_compatible
class NtedGovernmentLevel(models.Model):
    """
    """
    abbreviation = models.CharField(max_length=5, unique=True)
    level = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['abbreviation']

    def __str__(self):
        return self.level


@python_2_unicode_compatible
class NtedMissionArea(models.Model):
    """
    Abstract base class for FEMA Mission Area

    https://www.fema.gov/mission-areas
    """
    name = models.CharField(max_length=128)
    description = models.TextField()

    class Meta:
        abstract = True
        ordering = ['name']

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class NtedCoreCapability(models.Model):
    """
    Abstract base model for FEMA Core Capability.

    https://www.fema.gov/core-capabilities
    """
    name = models.CharField(max_length=128)
    description = models.TextField()

    class Meta:
        abstract = True
        ordering = ['name']

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class NtedCourse(models.Model):
    """
    """
    name = models.CharField(max_length=255)
    # TODO: Make sure number field is sutible for the new course numbers
    number = models.CharField(max_length=50, unique=True)
    # provider = models.ForeignKey('NtedTrainingProvider')
    description = models.TextField(blank=True)
    length = models.DecimalField(max_digits=6, decimal_places=2)
    # method =
    # level =
    continuing_education_units = models.DecimalField(max_digits=6,
                                                     decimal_places=2)
    objectives = models.TextField(blank=True)
    prerequisites = models.TextField(blank=True)
    # target_audience = models.ManyToManyField('NtedAudienceType')
    # training_certificates = models.TextField(blank=True)

    class Meta:
        abstract = True
        ordering = ['number']

    def __str__(self):
        return self.number


@python_2_unicode_compatible
class NtedInstructor(models.Model):
    """
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=100, blank=True)

    class Meta:
        abstract = True
        ordering = ['last_name', 'first_name']

    def __str__(self):
        return "{}, {}".format(self.last_name, self.first_name)

    def get_res_element(self, doc):
        element = doc.createElement('instructorpoc')
        element.setAttribute('instlastname', self.last_name)
        element.setAttribute('instfirstname', self.first_name)
        element.setAttribute('instphone', self.phone)
        if self.email:
            element.setAttribute('instemail', self.email)
        return element


@python_2_unicode_compatible
class NtedStudent(models.Model):
    """
    An abstract base class representing a NTED student.

    Override discipline_code and govnlevel_code to suit your particular implementation.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    sid = models.CharField('FEMA student ID', max_length=10, validators=[
        RegexValidator(
            regex=r'^[0-9]{10}$',
            message='Enter a valid FEMA student ID'
        ),
        RegexValidator(
            regex=r'^0000000000$',
            message='Enter a valid FEMA student ID',
            inverse_match=True
        )
    ])
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50)
    suffix = models.CharField(max_length=5, blank=True, choices=SUFFIXES)
    agency = models.CharField(max_length=225, blank=True)
    job_title = models.CharField(max_length=100, blank=True)
    address1 = models.CharField('address', max_length=100, blank=True)
    address2 = models.CharField('address line two', max_length=100, blank=True)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=2, choices=US_STATES + US_TERRITORIES + US_MILITARY)
    zipcode = models.CharField('ZIP code', max_length=5, validators=[
        RegexValidator(
            regex=r'^[0-9]{5}$',
            message='Enter a valid 5-digit ZIP code'
        ),
        RegexValidator(
            regex=r'^00000$',
            message='Enter a valid 5-digit ZIP code',
            inverse_match=True
        )
    ])
    country = models.CharField(max_length=2, choices=COUNTRIES, default='US')
    phone = models.CharField('work phone number', max_length=25)
    email = models.EmailField()
    #discipline
    #govnlevel

    class Meta:
        abstract = True
        ordering = ['created']

    def __str__(self):
        return "[{}] {} {}".format(self.sid, self.first_name, self.last_name)

    def discipline_code(self):
        """Override this method."""
        return 'OTH'

    def govnlevel_code(self):
        """Override this method."""
        return 'NA'

    def get_res_element(self, doc):
        element = doc.createElement('student')
        element.setAttribute('femasid', self.sid)
        if self.country == 'US':
            element.setAttribute('international', 'N')
            element.setAttribute('studentzipcode', self.zipcode)
            element.setAttribute('studentstate', self.state)
        else:
            element.setAttribute('international', 'Y')
            element.setAttribute('studentcountry', self.country)
        element.setAttribute('studentlastname', self.last_name)
        element.setAttribute('studentfirstname', self.first_name)
        if self.middle_name:
            element.setAttribute('studentmi', self.middle_name[:1])
        if self.agency:
            element.setAttribute('agency', self.agency)
        if self.job_title:
            element.setAttribute('title', self.job_title)
        if self.address1:
            element.setAttribute('address1', self.address1)
        if self.address2:
            element.setAttribute('address2', self.address2)
        element.setAttribute('studentcity', self.city)
        element.setAttribute('studentphone', self.phone)
        if self.email:
            element.setAttribute('studentemail', self.email)
        element.setAttribute('discipline', self.discipline_code())
        element.setAttribute('govnlevel', self.govnlevel_code())
        return element


@python_2_unicode_compatible
class NtedEvaluation(models.Model):
    """
    Abstract base model for FEMA Level 1 Student Assessment form.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    q1 = models.IntegerField(choices=KSA_CHOICES)
    q2 = models.IntegerField(choices=KSA_CHOICES)
    q3 = models.IntegerField(choices=SURVEY_CHOICES)
    q4 = models.IntegerField(choices=SURVEY_CHOICES)
    q5 = models.IntegerField(choices=SURVEY_CHOICES)
    q6 = models.IntegerField(choices=SURVEY_CHOICES)
    q7 = models.IntegerField(choices=SURVEY_CHOICES)
    q8 = models.IntegerField(choices=SURVEY_CHOICES)
    q9 = models.IntegerField(choices=SURVEY_CHOICES)
    q10 = models.IntegerField(choices=SURVEY_CHOICES)
    q11 = models.IntegerField(choices=SURVEY_CHOICES)
    q12 = models.IntegerField(choices=SURVEY_CHOICES)
    q13 = models.IntegerField(choices=SURVEY_CHOICES)
    q14 = models.IntegerField(choices=SURVEY_CHOICES)
    q15 = models.IntegerField(choices=SURVEY_CHOICES)
    q16 = models.IntegerField(choices=SURVEY_CHOICES)
    q17 = models.IntegerField(choices=SURVEY_CHOICES)
    q18 = models.IntegerField(choices=SURVEY_CHOICES)
    q19 = models.IntegerField(choices=SURVEY_CHOICES)
    q20 = models.IntegerField(choices=SURVEY_CHOICES)
    q21 = models.IntegerField(choices=SURVEY_CHOICES)
    q22 = models.IntegerField(choices=SURVEY_CHOICES)
    q23 = models.IntegerField(choices=SURVEY_CHOICES)
    q24 = models.TextField(blank=True)
    q25 = models.TextField(blank=True)
    q26 = models.TextField(blank=True)
    q27 = models.TextField(blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return 'Evaluation created {}'.format(self.created)

    def get_res_element(self, doc):
        element = doc.createElement('evaldata')
        for q in range(1, 24):
            el = doc.createElement('question')
            el.setAttribute('id', q)
            el.setAttribute('answer', getattr(self, 'q{}'.format(q)))
            element.appendChild(el)
        for q in range(24, 28):
            el = doc.createElement('comment')
            el.setAttribute('id', q)
            el.setAttribute('answer', getattr(self, 'q{}'.format(q)))
            element.appendChild(el)
        return element


@python_2_unicode_compatible
class FemaProfile(models.Model):
    fema_sid = models.CharField('FEMA student ID', max_length=10, validators=[
        RegexValidator(
            regex=r'^[0-9]{10}$',
            message='Enter a valid FEMA student ID'
        ),
        RegexValidator(
            regex=r'^0000000000$',
            message='Enter a valid FEMA student ID',
            inverse_match=True
        )
    ])
    created_at = models.DateTimeField()
    last_modified_at = models.DateTimeField()

    # Name
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50)
    suffix = models.CharField(max_length=5, blank=True, choices=SUFFIXES)
    maiden_name = models.CharField(max_length=50, blank=True)
    preferred_name = models.CharField(max_length=50, blank=True)

    # Contact Information
    work_email = models.EmailField()
    work_phone = models.CharField(max_length=25, blank=True)
    alternate_email = models.EmailField(blank=True)
    alternate_phone = models.CharField(max_length=25, blank=True)

    # Birth Information
    date_of_birth = models.DateField(null=True, blank=True)
    country_of_birth = models.CharField(
        max_length=2,
        choices=COUNTRIES,
        blank=True
    )
    city_of_birth = models.CharField(max_length=50, blank=True)
    state_of_birth = models.CharField(
        max_length=2,
        choices=US_STATES + US_TERRITORIES,
        blank=True
    )

    class Meta:
        abstract = True
        ordering = ['fema_sid']

    def __str__(self):
        return "Profile for {}".format(self.fema_sid)
